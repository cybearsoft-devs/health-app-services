"""CLI to upload images to S3"""
import click

from cli.utils import validate_extension


@click.group()
@click.pass_context
def entry_point(ctx):
    """
    Entrypoint for upload images to S3 commands.
    """
    ctx.ensure_object(dict)
    ctx.obj['ALLOWED_FILE_EXTENSIONS'] = AVAILABLE_EXS


@click.command()
@click.option('--file_path', prompt='File path', callback=validate_extension, type=click.Path(exists=True),
              help='Absolute path to file with images to be uploaded to S3.')
@click.option('--region', prompt='S3 region', type=str,
              default='qwerty', help='AWS S3 bucket region to store images in.')
@click.option('--bucket', prompt='S3 bucket name', type=str,
              default='qwerty', help='AWS S3 bucket name to store images in.')
@click.option('--folder', prompt='S3 bucket folder', type=str,
              default='qwerty', help='AWS S3 bucket folder path to store images in.')
@click.option('--image_extension', type=str,
              default='jpg', help='File extension to save image with.')
@click.option('--min_row', type=int,
              default=2, help='Row number to start data extraction from.')
@click.option('--name_col_num', type=int,
              default=0, help='Column number to extract file name from.')
@click.option('--url_col_num', type=int,
              default=4, help='Column number to extract file url from.')
def upload_images(file_path, region, bucket,
                  folder, image_extension,
                  min_row, name_col_num,
                  url_col_num):
    """
    Command to upload images to S3.
    """
    upload_images_batch = UploadImages(file_path=file_path, folder=folder,
                                       region=region, bucket=bucket)

    upload_images_batch.process(min_row=min_row, name_col_num=name_col_num,
                                url_col_num=url_col_num, image_ext=image_extension)


if __name__ == "__main__":
    from services.file_service import AVAILABLE_EXS
    from batches.upload_images import UploadImages

    entry_point.add_command(upload_images)

    entry_point()
