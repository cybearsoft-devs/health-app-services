import os
import click

from app.app_config_helper import AppConfigHelper


def skip_prompts(ctx, _, value):
    if value:
        for p in ctx.command.params:
            if isinstance(p, click.Option) and p.prompt is not None:
                p.prompt = None
    return value


def configure_defaults(ctx, _, value):
    if value:
        env = os.environ.get('ENV', 'development')
        region = os.environ.get('AWS_REGION', os.environ.get('AWS_DEFAULT_REGION', 'qwerty'))

        config_helper = AppConfigHelper(env=env, region=region, seconds_before_refresh=3600)
        config = config_helper.config

        for param in ctx.command.params:
            if param.envvar in config.keys():
                param.default = config[param.envvar]

    return value


def validate_extension(ctx, _, value):
    allowed_extensions = ctx.obj['ALLOWED_FILE_EXTENSIONS']
    file_extension = value.split('.')[-1]
    if file_extension not in allowed_extensions:
        raise click.BadParameter(f'Inappropriate file extension. Available options are: {allowed_extensions}')
    return value
