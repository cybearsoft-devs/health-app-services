"""PopulateFoodIndex batch CLI logic"""
import click

from cli.utils import skip_prompts, configure_defaults


@click.group()
def entry_point():
    """
    Entrypoint for UpdateFoods batch commands.
    """
    pass


@click.command()
@click.option('-q/--quiet', default=False, callback=skip_prompts, is_eager=True, expose_value=False,
              help='Disable all prompts.')
@click.option('-c/--config', default=False, callback=configure_defaults, is_eager=True, expose_value=False,
              help='Read option defaults from environment variables.')
@click.option('--auth_token', prompt='Authorization token', type=str,
              envvar='CALORIE_KING_API_TOKEN',
              help='API authorization token.', )
@click.option('--es_host', prompt='Elasticsearch Host', type=str,
              default='http://0.0.0.0:9200',
              envvar='ELASTICSEARCH_HOST',
              help='Elasticsearch host.')
@click.option('--api_host', prompt='API Host', type=str,
              default='https://foodapi.calorieking.com',
              envvar='CALORIE_KING_HOST',
              help='Base URL for the api.')
@click.option('--fields', prompt='Fields',
              default='$detailed, category',
              envvar='CALORIE_KING_FIELDS',
              help='Selector to specify the fields to include in partial responses.')
@click.option('--batch_size', prompt='Batch size', type=int,
              default=1000,
              envvar='FOOD_INDEX_BATCH_SIZE',
              help='The number of results to include in requests when paging.')
@click.option('--op_type', prompt='Elasticsearch operation type', type=str,
              default='create',
              help='Elasticsearch operation type. '
                   'Set to "create" to only index the document if it does not already exist.'
                   'To index new documents and reindex existing ones, use "index" operation type.')
@click.option('--time_to_sleep', prompt='Time to sleep', type=int,
              default=0,
              envvar='FOOD_INDEX_TIME_TO_SLEEP',
              help='Amount of time to sleep between CalorieKing API requests.')
def process(auth_token, es_host, api_host, fields, op_type, batch_size, time_to_sleep):
    """
    Command to run full bulk data indexing in Elasticsearch.
    Data is being received from CalorieKing API.
    """
    print(es_host)
    populate_food_index_batch = PopulateFoodIndex(auth_token=auth_token,
                                                  es_host=es_host,
                                                  api_host=api_host,
                                                  batch_size=batch_size,
                                                  time_to_sleep=time_to_sleep)
    populate_food_index_batch.process(fields=fields, op_type=op_type)


if __name__ == "__main__":
    from batches.populate_food_index import PopulateFoodIndex

    entry_point.add_command(process)

    entry_point()
