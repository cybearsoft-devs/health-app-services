"""CalorieKingService service CLI logic"""
import click

from cli.utils import skip_prompts, configure_defaults


@click.group()
def entry_point():
    """
    Entrypoint for CalorieKingService service commands.
    """
    pass


@click.command()
@click.option('-q/--quiet', default=False, callback=skip_prompts, is_eager=True, expose_value=False,
              help='Disable all prompts.')
@click.option('-c/--config', default=False, callback=configure_defaults, is_eager=True, expose_value=False,
              help='Read option defaults from environment variables.')
@click.option('--host', default='https://foodapi.calorieking.com', prompt='API Host',
              envvar='CALORIE_KING_HOST',
              help='Base URL for the api.')
@click.option('--auth_token', prompt='Authorization token',
              envvar='CALORIE_KING_API_TOKEN',
              help='Authorization token.')
@click.option('--fields', default='$detailed, category', prompt='Fields',
              envvar='CALORIE_KING_FIELDS',
              help='Selector to specify the fields to include in partial responses.')
@click.option('--limit', default=20, prompt='Limit',
              help='The number of results to include in requests when paging.')
@click.option('--offset', default=0, prompt='Offset',
              help='The index (zero-based) of the first result to return when paging.')
def get_foods(host, auth_token, fields, limit, offset):
    """
    Command to return a collection of foods in the CalorieKing database.
    """
    calorie_king_service = CalorieKingService(host=host, auth_token=auth_token)
    response = calorie_king_service.get_foods(fields=fields, limit=limit, offset=offset)
    for item in response:
        click.echo(item)


if __name__ == "__main__":
    from services.calorie_king_service import CalorieKingService

    entry_point.add_command(get_foods)

    entry_point()
