"""ElasticSearchService service CLI logic"""
import os
import re
import json
import click
from enum import Enum

from cli.utils import configure_defaults, skip_prompts

API_DATA_SOURCE = 'api'
FILE_DATA_SOURCE = 'file'
DataSources = Enum('data_sources', (FILE_DATA_SOURCE, API_DATA_SOURCE))

ROOT_DIRECTORY = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEFAULT_FILE_PATH = os.path.join(ROOT_DIRECTORY, 'seeds/foods.json')


@click.group()
def entry_point():
    """
    Entrypoint for ElasticSearchService service commands.
    """
    pass


@click.command()
@click.option('-q/--quiet', default=False, callback=skip_prompts, is_eager=True, expose_value=False,
              help='Disable all prompts.')
@click.option('-c/--config', default=False, callback=configure_defaults, is_eager=True, expose_value=False,
              help='Read option defaults from environment variables.')
@click.option('--es_url', default='http://0.0.0.0:9200', prompt='Elasticsearch Host',
              envvar='ELASTICSEARCH_HOST',
              help='Elasticsearch url.')
@click.option('--index', default='foods', prompt='Index',
              envvar='FOOD_INDEX',
              help='The index to search in.')
@click.option('--query', prompt='Query',
              help='The query to search.')
@click.option('--size', default=10, prompt='Size',
              help='The number of results to return.')
@click.option('--from_', default=0, prompt='From',
              help='The index (zero-based) of the first result to return when paging.')
@click.option('--fields', default='name^2, brand.name^1, classification', prompt='Fields',
              help='Fields to search against (Example: name, brand.name).')
def fuzzy_search(es_url, index, query, size, from_, fields):
    """
    Command to run fuzzible data search in Elasticsearch.
    """
    fields = re.sub(r'\s+', '', fields).split(',')
    es_service = ElasticSearchService(host=es_url)
    search_result = es_service.fuzzy_search(index=index, query=query, fields=fields,
                                            size=size, from_=from_)
    for item in search_result:
        click.echo(item)


@click.command()
@click.option('-q/--quiet', default=False, callback=skip_prompts, is_eager=True, expose_value=False,
              help='Disable all prompts.')
@click.option('-c/--config', default=False, callback=configure_defaults, is_eager=True, expose_value=False,
              help='Read option defaults from environment variables.')
@click.option('--es_url', default='http://0.0.0.0:9200', prompt='Elasticsearch Host',
              envvar='ELASTICSEARCH_HOST',
              help='ElasticSearch url.')
@click.option('--host', default='https://foodapi.calorieking.com', prompt='API Host',
              envvar='CALORIE_KING_HOST',
              help='Base URL for the api.')
@click.option('--auth_token', prompt='Authorization token',
              envvar='CALORIE_KING_API_TOKEN',
              help='Authorization token.')
@click.option('--fields', default='$detailed, category', prompt='Fields',
              envvar='CALORIE_KING_FIELDS',
              help='Selector to specify the fields to include in partial responses.')
@click.option('--limit', default=20, prompt='Limit',
              help='The number of results to include in requests when paging.')
@click.option('--offset', default=0, prompt='Offset',
              help='The index (zero-based) of the first result to return when paging.')
@click.option('--index', default='foods', prompt='Index',
              envvar='FOOD_INDEX',
              help='The index to bulk index data in.')
@click.option('--id_key', default='foodId', prompt='ID Key',
              help='Key to use for document id field value extraction.')
@click.option('--op_type', default='create', prompt='Operation type',
              help='Elasticsearch operation type. '
                   'Set to "create" to only index the document if it does not already exist.'
                   'To index new documents and reindex existing ones, use "index" operation type.')
@click.option('--required_fields', default=['name', 'mass', 'nutrients.energy'], prompt='Required fields paths',
              type=list,
              help='Required document fields paths. '
                   'In case there are no any of these fields or their values are null document will not be indexed.')
@click.option('--file_path', prompt='File path', default=DEFAULT_FILE_PATH,
              help='File path to extract data from.')
@click.option('--data_source', default='file', prompt='Data source',
              type=click.Choice(list(map(lambda x: x.name, DataSources))),
              help='Data source to fetch data from for indexing.')
def bulk_index(es_url, host, auth_token,
               fields, limit, offset,
               index, id_key, op_type,
               required_fields, file_path, data_source):
    """
    Command to run bulk data index in Elasticsearch with data from json file.
    """
    documents = []
    es_service = ElasticSearchService(host=es_url)

    if data_source == FILE_DATA_SOURCE:
        with open(file_path, 'r') as f:
            documents = json.load(f)

    elif data_source == API_DATA_SOURCE:
        calorie_king_service = CalorieKingService(host=host, auth_token=auth_token)
        _, documents = calorie_king_service.get_foods(fields=fields, limit=limit, offset=offset)

    if op_type == INDEX_OP_TYPE:
        es_service.delete_index(index)

    click.echo(es_service.bulk_index(index=index, documents=documents, id_key=id_key, op_type=op_type,
                                     required_fields=required_fields))


if __name__ == "__main__":
    from services.elasticsearch_service import ElasticSearchService, INDEX_OP_TYPE
    from services.calorie_king_service import CalorieKingService

