from datetime import timedelta
from random import randint
from typing import Optional

from flask import current_app

from app import is_development, is_staging, is_testing
from services.email_service import send_email, EmailTemplate
from services.sms_service import send_sms


OTP_MESSAGE = ("OTP: {}. Please use this OTP to login to your account. "
               "This code expires in {} minutes.")


class OTPService:
    FAKE_OTP = "1111"
    VALIDATOR_PHONE_NUMBER = "0000000000000"

    OTP_KEY = "otp:{otp}"
    OTP_PHONE_NUMBER_KEY = "phone_number:{phone_number}"
    RETRY_AFTER_KEY = "otp_retry_after:{phone_number}"
    TIMER_KEY = "otp_timer:{phone_number}"

    def _is_internal_email(email):
        if email.lower().endswith("qwerty"):
            return True
        if email.lower().endswith("@itfaq.global"):
            return True
        return False

    def _is_outside_UAE(phone_number):
        return not phone_number.startswith("971")

    @classmethod
    def create_new_otp(cls, phone_number):
        """
        Creates new unique OTP for specified phone number.
        :param phone_number: user' phone number
        :return: new OTP
        """
        from app import redis_client
        exp_time = timedelta(minutes=current_app.config.get("OTP_DURATION_IN_MINUTES", 30))
        phone_number_key = cls.OTP_PHONE_NUMBER_KEY.format(phone_number=phone_number)

        # In order the validators of our app by Apple, Google and Huwaei to be able to test our app
        if phone_number == cls.VALIDATOR_PHONE_NUMBER:
            otp = cls.FAKE_OTP
            otp_key = cls.OTP_KEY.format(otp=otp)
        else:
            while True:
                otp = randint(1000, 9999)
                otp_key = cls.OTP_KEY.format(otp=otp)
                if not redis_client.exists(otp_key) and otp != cls.FAKE_OTP:
                    break
        redis_client.set(otp_key, "", ex=exp_time)
        redis_client.set(phone_number_key, otp, ex=exp_time)
        return otp

    @classmethod
    def delete_otp(cls, phone_number):
        """
        Deletes user' OTP.
        :param phone_number: user' phone number
        :return: None
        """
        from app import redis_client

        phone_number_key = cls.OTP_PHONE_NUMBER_KEY.format(phone_number=phone_number)
        otp = redis_client.get(phone_number_key)
        redis_client.delete(phone_number_key)

        otp_key = cls.OTP_KEY.format(otp=otp)
        redis_client.delete(otp_key)

    @classmethod
    def validate_otp(cls, otp, phone_number):
        from app import redis_client
        phone_number_key = cls.OTP_PHONE_NUMBER_KEY.format(phone_number=phone_number)

        if otp != redis_client.get(phone_number_key):
            return False, "Incorrect OTP"

        return True, None

    @classmethod
    def expire_otp(cls, phone_number, exp):
        from app import redis_client
        phone_number_key = cls.OTP_PHONE_NUMBER_KEY.format(phone_number=phone_number)
        otp = redis_client.get(phone_number_key)
        otp_key = cls.OTP_KEY.format(otp=otp)

        redis_client.expire(phone_number_key, exp)
        redis_client.expire(otp_key, exp)

    @classmethod
    def expire_timer(cls, phone_number, exp):
        from app import redis_client
        timer_key = cls.TIMER_KEY.format(phone_number=phone_number)
        redis_client.expire(timer_key, exp)

    @classmethod
    def get_retry_after(cls, phone_number) -> Optional[int]:
        """
        Returns time to retry OTP request for specific phone number.
        :param phone_number: user' phone number
        :return: time to retry request after
        """
        from app import redis_client

        time_to_retry = cls.TIMER_KEY.format(phone_number=phone_number)
        retry_after = redis_client.ttl(time_to_retry)

        if retry_after < 0:
            return 0

        return retry_after

    @classmethod
    def update_time_to_retry(cls, phone_number, otp):
        """
        Updates time to retry OTP request for specific phone number.
        :param phone_number: user' phone number
        :param otp: user' otp
        :return: new time to retry request after
        """
        from app import redis_client

        retry_after = cls._update_timer(phone_number=phone_number)

        time_to_retry = cls.TIMER_KEY.format(phone_number=phone_number)
        redis_client.set(time_to_retry, otp, ex=retry_after)

        return retry_after

    @classmethod
    def _update_timer(cls, phone_number):
        """
        Updates timer after each time user sends an OTP.
        :param phone_number: user' phone number
        :return: new time to retry request after
        """
        from app import redis_client
        retry_after = 5
        exp_time = timedelta(minutes=current_app.config.get("OTP_DURATION_IN_MINUTES", 30))

        timer = cls.RETRY_AFTER_KEY.format(phone_number=phone_number)

        if (current_timer := redis_client.get(timer)) is not None:
            if is_development() or is_staging():
                retry_after = 1 + int(current_timer)
            else:
                retry_after = 2 * int(current_timer)
        redis_client.set(timer, retry_after, ex=exp_time)

        return retry_after

    @classmethod
    def send_otp(cls, full_name, email, phone_number, otp):
        from run import app
        if is_testing():
            return True

        if app.config["SEND_OTP_BY_MAIL"]:
            send_email(email=email, template_id=EmailTemplate.SEND_OTP.value, params={
                    "otp": str(otp),
                    "full_name": full_name,
                    "otp_expire": str(app.config["OTP_DURATION_IN_MINUTES"])
                })

        message = OTP_MESSAGE.format(otp, app.config["OTP_DURATION_IN_MINUTES"])
        send_sms(phone_number=phone_number, message=message)

        return True

    @classmethod
    def send_otp_with_phone_number(cls, phone_number, otp):
        from run import app
        if is_testing():
            return True

        message = OTP_MESSAGE.format(otp, app.config["OTP_DURATION_IN_MINUTES"])
        send_sms(phone_number=phone_number, message=message)

        return True
