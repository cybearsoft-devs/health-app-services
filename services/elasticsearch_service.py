"""Elasticsearch service logic"""
from typing import Dict, Any, Tuple, Union, List

from elasticsearch import Elasticsearch, helpers

INDEX_OP_TYPE = 'index'
CREATE_OP_TYPE = 'create'
OP_TYPES = (INDEX_OP_TYPE, CREATE_OP_TYPE)


class ElasticSearchException(Exception):
    pass


class ElasticSearchService(object):
    """
    Class to interact with Elasticsearch.
    """

    def __init__(self, host: str = None) -> None:
        """
        ElasticSearchService initialization
        Parameters:
            host (str): elasticsearch host
        """
        if not host:
            host = 'http://elasticsearch:9200'

        self._conn = Elasticsearch([host])

    @staticmethod
    def _find_value_by_path(path, dict_):
        keys = path.split('.')
        rv = dict_
        for key in keys:
            rv = rv.get(key) if rv else None
        return rv

    def delete_index(self, index):
        return self._conn.indices.delete(index=index, ignore=[400, 404])

    def bulk_index(self,
                   index: str,
                   documents: List[dict],
                   id_key: str,
                   required_fields: list,
                   op_type: str = CREATE_OP_TYPE) -> Tuple[int, Union[int, List[Any]]]:
        """
        Creates a documents from provided list of dicts
        Parameters:
            index (str): elasticsearch index name to update data in.
            documents (List[dict]): list of documents to be indexed in elasticsearch.
            id_key (str): key to use for document id field value extraction
            required_fields (list): required document fields paths
            op_type (str): elasticsearch operation type
        Returns:
            Tuple[int, Union[int, List[Any]]]
        """
        def actions():
            for doc in documents:
                if all([self._find_value_by_path(field, doc) for field in required_fields]):
                    yield dict(_index=index, _source=doc, _id=doc[id_key], _op_type=op_type)

        if op_type not in OP_TYPES:
            raise ElasticSearchException(f'Operation type "{op_type}" is not allowed.')

        try:
            return helpers.bulk(client=self._conn, actions=actions(), raise_on_error=False)
        except Exception as e:
            raise ElasticSearchException(f'Failed to bulk index.') from e

    def fuzzy_search(self, index: str, query: str, fields: List[str], size: int = 10, from_: int = 0) -> List:
        """
        Returns results matching a fuzzible query.
        Parameters:
            index (str): elasticsearch index name to search data in.
            query (str): query to search data for.
            fields (List[str]): list of fields names to search against
            size (int): the number of results to return
            from_ (int): the index (zero-based) of the first result to return when paging
        Returns:
            List
        """
        query = dict(
             multi_match=dict(
                 fields=fields,
                 query=query,
                 fuzziness="AUTO"
             )
        )
        try:
            query_result = self._conn.search(index=index, query=query, size=size, from_=from_)
            return list(map(lambda hit: hit, query_result['hits']['hits']))
        except Exception as e:
            raise ElasticSearchException(f'Failed to search for {query}.') from e
