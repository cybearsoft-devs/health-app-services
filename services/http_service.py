"""HTTP service logic"""
import time
from typing import Dict
from urllib.parse import urljoin
from flask import current_app
import requests
from app.event_logger import EventLogger
from models.types import Action
from services.sns_service import SNSService

class HTTPServiceException(Exception):
    pass

class RefreshTokenException(Exception):
    pass


class HTTPService:
    """
    Class to interact with 3rd party services via HTTP.
    """

    def __init__(self, host: str = None, headers: Dict = None) -> None:
        """
        HTTPService initialization
        Parameters:
            host (str): base URL for the api
        Returns:
            None
        """
        self.headers = {'Accept': 'application/json', 'Content-Type': 'application/json', **headers}

        self.host = host
        self.session = requests.Session()

    def _request(self,
                 path: str,
                 method: str = 'GET',
                 params: Dict = None,
                 stream: bool = False,
                 data: object = None) -> requests.Response:
        """
        Sends request to 3rd party service
        Parameters:
            path (str): endpoint to request or full url
            method (str): http method to use
            params (Dict): params to pass in http request
            stream (bool): whether to immediately download the response content
        Returns:
            Dict
        """
        url = urljoin(self.host, path) if self.host else path

        try:
            response = self.session.request(
                method, url, headers=self.headers, params=params, stream=stream, data=data
            )
        except Exception as e:
            raise HTTPServiceException('Failed to request 3rd party service.') from e

        if response.ok:
            return response

        elif response.status_code == 429:
            retry_after = int(response.headers.get('Retry-After', 0))
            for i in range(retry_after, 0, -1):
                time.sleep(1)
                print(f'Too many requests. Retry after: {i}')

            return self._request(path=path, method=method, params=params, stream=stream, data=data)
        print("Error in response")
        print(response.text)
        raise HTTPServiceException(response)

    def content(self, url: str) -> bytes:
        """
        Receives content of the response, in bytes
        Parameters:
            url (str): full url to be requested
        Returns:
            bytes
        """
        return self._request(url, stream=True).content
