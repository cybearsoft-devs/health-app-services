"""Calorie King API service logic"""
from typing import Tuple, Any, Optional

from services.http_service import HTTPService


class CalorieKingException(Exception):
    pass


class CalorieKingService(HTTPService):
    """
    Class to interact with Calorie King API.
    """

    def __init__(self, auth_token: str, host: str = None) -> None:
        """
        CalorieKingService initialization
        Parameters:
            auth_token (str): authorization token
            host (str): base URL for the api (e.g. https://foodapi.calorieking.com)
        Returns:
            None
        """
        if not host:
            host = 'https://foodapi.calorieking.com'

        headers = {'Authorization': f'Basic {auth_token}'}

        super().__init__(host=host, headers=headers)

    def get_foods(self, fields: str = None, limit: int = 20, offset: int = 0) -> Tuple[Any, Optional[Any]]:
        """
        Returns a collection of foods in the CalorieKing database.
        Parameters:
            fields (str): selector to specify the fields to include in partial responses
            limit (int): the number of results to include in requests when paging
            offset (int): the index (zero-based) of the first result to return when paging
        Returns:
            Tuple[Any, Optional[Any]]
        """
        if not fields:
            fields = '$detailed'

        params   = {'fields': fields, 'limit': limit, 'offset': offset}
        response = self._request('/v1/foods', params=params).json()

        metadata = response.get('metadata')
        data     = response.get('foods')

        return metadata, data
