"""Edamam API service logic"""
import json

import jmespath
from flask import current_app

from services.http_service import HTTPService


class EdamamException(Exception):
    pass


class EdamamFoodData(object):
    def __init__(self, food):
        self.food = food
        measures = self.food.get("measures", [])
        self.measure = self._find_measures(measures)

    def _calculate_nutrient(self, nutrient):
        return nutrient * (self.grams / 100)

    def _find_measures(self, measures):
        per_label = {}
        for mea in measures:
            per_label[mea.get("label")] = mea
        for sorted_label in ["Serving", "Cup", "Whole"]:
            if (val := per_label.get(sorted_label)) is not None:
                return val
        # We need to search for a measure not small
        for mea in measures:
            if mea.get("weight") > 1:
                return mea
        # If we don't have choice we take the first one
        if len(measures) > 0:
            return measures[0]
        # Default value
        return {}


    @property
    def elastic_food_id(self):
        return jmespath.search('food.foodId', self.food)

    @property
    def name(self):
        return jmespath.search("food.label", self.food)

    @property
    def image_url(self):
        return jmespath.search("food.image", self.food)

    @property
    def grams(self):
        return self.measure.get("weight", 0)

    @property
    def quantity(self):
        return jmespath.search("quantity", self.food) or 1

    @property
    def measures_uri(self):
        return self.measure.get("uri", 'http://www.edamam.com/ontologies/edamam.owl#Measure_serving')

    @property
    def energy(self):
        return int(self._calculate_nutrient(jmespath.search('food.nutrients.ENERC_KCAL', self.food))) or 1

    @property
    def protein(self):
        return self._calculate_nutrient(jmespath.search('food.nutrients.PROCNT', self.food))

    @property
    def fat(self):
        return self._calculate_nutrient(jmespath.search('food.nutrients.FAT', self.food))

    @property
    def net_carbs(self):
        return self._calculate_nutrient(jmespath.search('food.nutrients.CHOCDF', self.food))

    @property
    def known_as(self):
        return jmespath.search("food.knownAs", self.food)


class EdamamService(HTTPService):
    """
    Class to interact with Edamam API.
    """

    def __init__(self) -> None:
        """
        EdamamService initialization
        Parameters:
        Returns:
            None
        """
        self.headers = {}
        self.host = current_app.config['EDAMAM_HOST']
        self.app_id = current_app.config['EDAMAM_APP_ID']
        self.app_key = current_app.config['EDAMAM_API_KEY']

        super().__init__(host=self.host, headers=self.headers)

    def _unique_elements_with_image(self, arr):
        """
        Return an array with only unique name + foodId elements
        """    
        dic = {}
        for item in arr:
            item = EdamamFoodData(item)
            if item.image_url:
                dic[f"{item.elastic_food_id}-{item.known_as}"] = item
        return list(dic.values())

    def get_foods(self,
                  ingr: str,
                  category: str = 'generic-foods',
                  nutrition_type: str = 'logging') -> list:
        """
        Returns a collection of foods in the Edamam database.
        Parameters:
            ingr (str): a keyword search parameter to be found in the food name
            category (str): food category
            nutrition_type (str): select between cooking and food logging processor
        Returns:
            list
        """
        params = {
            'app_id': self.app_id,
            'app_key': self.app_key,
            'ingr': ingr,
            'nutrition-type': nutrition_type,
            'category': category,
        }
        url = '/api/food-database/v2/parser'
        current_app.logger.info(f"Calling Edamam {url} {params}")
        try:
            response = self._request(url, params=params).json()
            parsed = response.get('parsed', [])
            hints = response.get('hints', [])
            return self._unique_elements_with_image([*parsed, *hints])
        except Exception as e:
            return []

    def get_food(self, food_id: str) -> EdamamFoodData:
        """
        Returns a single food item in the Edamam database.
        Parameters:
            food_id (str): Edamam food id
        Returns:
            EdamamFoodData
        """
        foods = self.get_foods(ingr=food_id)
        return jmespath.search('[0]', foods)

    def get_food_nutrients(self,
                           food_id: str,
                           quantity: int,
                           measure_uri) -> dict:
        """
        Returns a collection of foods nutrients in the Edamam database.
        Parameters:
            food_id (str): edamam food id
            quantity (str): food number of portions
            measure_uri (str): measure URI provided by parser
        Returns:
            dict
        """
        params = {
            'app_id': self.app_id,
            'app_key': self.app_key,
        }
        data = {
            'ingredients': [
                {
                    'quantity': quantity,
                    'measureURI': measure_uri,
                    'foodId': food_id
                }
            ]
        }
        url = '/api/food-database/v2/nutrients'
        current_app.logger.info(f"Calling Edamam {url} {data}")
        response = self._request(url,
                                 method='POST',
                                 params=params,
                                 data=json.dumps(data))
        response = response.json()
        return response['totalNutrients']
